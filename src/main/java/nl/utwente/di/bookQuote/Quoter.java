package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    public double getBookPrice(String isbn) {
        HashMap<String, Double> bookPrice = new HashMap<>();
        bookPrice.put("1", 10.0);
        bookPrice.put("2", 45.0);
        bookPrice.put("3", 20.0);
        bookPrice.put("4", 35.0);
        bookPrice.put("5", 50.0);
        double price = 0.0;
        if (bookPrice.containsKey(isbn)) {
            price = bookPrice.get(isbn);
        }
        return price;
    }
}
